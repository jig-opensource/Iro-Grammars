# Change Log

All notable changes to the "mindcode" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]


## [0.0.1] - 210328
### Added
- Initial release

## [0.0.2] - 210328
### Changed
- Syntax Colors adjusted




#### !i

https://keepachangelog.com/de/1.0.0/

