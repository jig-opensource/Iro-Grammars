# mindcode README

<<<<<<< HEAD
This package contains an VSCode Extension to highlight the Mindcode source code.
Mindcode is a compiler for the Game Mindustry and it's prgrammable processors / logic.
=======
This is the README for your extension "mindcode". 

# Installation
Copy it to:
c:\Users\<user>\AppData\Local\Programs\Microsoft VS Code\resources\app\extensions\mindcode\


>>>>>>> origin/master

François Beausoleil created the very useful compiler mindcode:

* [His product presentation](https://www.reddit.com/r/Mindustry/comments/m60qli/mindcode_a_higher_level_language_that_compiles/)
* [Synax](https://github.com/francois/mindcode/blob/main/SYNTAX.markdown)
* [Examples and the online Compiler](https://mindcode.herokuapp.com)



## Features

- Mindcode Grammar / Syntax for the Mindustry Game Language Compiler
- The Default Dark+ Mindcode which adds some additional Syntax Highlightings


## Requirements
.

## Extension Settings
.

## Known Issues

The syntax/grammar definition is designed more openly than mindcode, because it could be that mindcode will still be extended and I want to have little effort to extend the syntax/grammar later.

Therefore, it can happen that the editor does not show an error, but later the compiler does.

## Release Notes

See: CHANGELOG.md

